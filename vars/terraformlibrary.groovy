#!/usr/bin/env groovy

def commonTerraformFunction(Map stepParams) {
    dir("$stepParams.path") {
        sh "$stepParams.command"
    }
}
