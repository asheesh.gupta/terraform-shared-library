#!/usr/bin/env groovy

def errorInfo(Map stepParams) {
    slackSend channel: "${stepParams.slack_channel}",
    color: "danger",
    message: "Message :- ${stepParams.message}\n JOB_NAME:- ${env.JOB_NAME}\n BUILD_URL:- ${env.BUILD_URL} "
}

def successInfo(Map stepParams) {
    slackSend channel: "${stepParams.slack_channel}",
    color: "good",
    message: "Message:- ${stepParams.message}\n JOB_NAME:- ${env.JOB_NAME}\n BUILD_URL:- ${env.BUILD_URL}"
}
