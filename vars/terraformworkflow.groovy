#!/usr/bin/env groovy
def call(Map stepParams) {
    checkout scm
    readYaml(filepath: "${stepParams.file}")  
    terraformFmt()
    terraformInit()
    terraformValidate()
    terraformPlan()
    terraformApply()
    successStage()
}

def readYaml(Map stepParams) {
    try {
        config = commonproperties.readYamlFile(file: "${stepParams.filepath}")
    }
   catch(e) {
        errorFunction(
            message : "error in reading file",
            slack_channel : "$config.SLACK_CHANNEL",
            )
        }    
}

def terraformFmt() {
    stage('Terraform Lint Checking') {
    try {
    terraformlibrary.commonTerraformFunction(
        path: "$config.PATH",
        command: "terraform fmt"
        )
    }
    catch(e) {
        errorFunction(
            message : "error in formatting",
            slack_channel : "$config.SLACK_CHANNEL",
            )
        } 
    }   
}

def terraformValidate() {
    stage('Terraform Validate') {
    try {
    terraformlibrary.commonTerraformFunction(
         path: "$config.PATH",
        command: "terraform validate"
        )
    }
    catch(e) {
        errorFunction(
            message : "error in validating",
            slack_channel : "$config.SLACK_CHANNEL",
            )
        }
    }
}

def terraformInit() {
    stage('Terraform Initialization') {
    try {
    terraformlibrary.commonTerraformFunction(
         path: "$config.PATH",
        command: "terraform init"
        )
    }
    catch(e) {
        errorFunction(
            message : "error in Initialization",
            slack_channel : "$config.SLACK_CHANNEL",
            )
        }
    }
}

def terraformPlan() {
    stage('Terraform Plan') {
    try {
    terraformlibrary.commonTerraformFunction(
         path: "$config.PATH",
        command: "terraform plan"
        )
    }
    catch(e) {
        errorFunction(
            message : "error in executing plan",
            slack_channel : "$config.SLACK_CHANNEL",
            )
        }
    }
}

def terraformApply() {
    stage('Terraform Apply') {
    try {
    terraformlibrary.commonTerraformFunction(
        path: "$config.PATH",
        command: "terraform apply -auto-approve"
    )
    }
    catch(e) {
        errorFunction(
            message : "error in applying",
            slack_channel : "$config.SLACK_CHANNEL",
        )
    }
    }
}

def errorFunction(Map stepParams) {
 notification.errorInfo(
    message: "$stepParams.message",
    slack_channel: "$stepParams.slack_channel",
    )
}

def successStage() {
    stage ('Success Slack Notification') {
    notification.successInfo(
        message: "Successfully Executed the terraform code",
        slack_channel: "$config.SLACK_CHANNEL",
        )
    }
}
