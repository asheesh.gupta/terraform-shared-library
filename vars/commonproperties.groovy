#!/usr/bin/env groovy

def readYamlFile(Map stepParams) {
    stage("Reading Yaml Configuration File") {
    config = readYaml file: "${stepParams.file}" 
    return config
    }
}
