module "vpc" {
  source            = "./modules/VPC"
  cidr_block_value  = var.cidr_block
  subnet_for_nat_gw = module.Subnets.subnet_ids
}

module "Subnets" {
  source      = "./modules/Subnets/"
  vpc_id      = module.vpc.vpc_id
  subnet_cidr = var.private_subnet_cidr
  sub_az      = var.sub_az
}

module "PrivateSubnets" {
  source      = "./modules/Subnets/"
  vpc_id      = module.vpc.vpc_id
  subnet_cidr = var.public_subnet_cidr
  sub_az      = var.sub_az
}


module "publicRouteTable" {
  source      = "./modules/RouteTable/"
  vpc_id      = module.vpc.vpc_id
  gateway_id  = module.vpc.igw_id
  subnet_cidr = module.Subnets.subnet_ids
  name        = var.routeTable
}
module "privateRouteTable" {
  source      = "./modules/RouteTable/"
  vpc_id      = module.vpc.vpc_id
  gateway_id  = module.vpc.ngw_id
  subnet_cidr = module.PrivateSubnets.subnet_ids
  name        = var.routeTable
}

module "securityGroup" {
  source           = "./modules/SecurityGroup"
  vpc_id           = module.vpc.vpc_id
  cidr_blocks      = var.securityGroupVar
  name_sg          = var.name_sg
  name_description = var.name_description
  to_port          = var.to_port
  from_port        = var.from_port
  protocol_name    = var.protocol_name
  tag_name         = var.tag_name
}

/*module "alb" {
  source = "./modules/ALB"
  vpc_id = module.vpc.vpc_id
  security_group_id = module.securityGroup.security_group_id
  subnet_id = module.Subnets.subnet_ids  
}
*/