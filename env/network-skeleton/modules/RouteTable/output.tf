output "main_route_table_id" {
  description = "The main_route_table_id of the VPC"
  value       = aws_route_table.route_table.id
}
