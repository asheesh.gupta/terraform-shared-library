# Module for Route Table

- To include this module in your code

source = " "       #path where you have stored the module

# Variables that  you need to passed while calling the route table module

```
 source = "./modules/RouteTable/"  
  vpc_id = module.vpc.vpc_id        # Vpc id of your vpc
  gateway_id = module.vpc.igw_id    # Gateway id like i have given internet gatway id in this route table this is for public route table
  subnet_cidr = module.Subnets.public_subnet_ids  # subnet cidr block 
```
### When we want to create a private subnet

```
  module "privateRouteTable" {
  source = "./modules/RouteTable/"
  vpc_id = module.vpc.vpc_id       # vpc id of your vpc
  gateway_id = module.vpc.ngw_id   # gateway id i have given nat gateway id here for my private route table
  subnet_cidr = module.Subnets.private_subnet_ids   # subnet cidr blocks
  name = var.routeTable  # this is used just to give the tag 
}
```

## Inputs       

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| vpc_id | Id of your vpc | list | null| yes |
| subnet_cidr | subnet cidr blocks | List | null | yes |
| gateway_id | Gateway you want to attach ie. "nat gateway" | list | null | yes |
| name | Name of the tag | string | yes | yes |     
  

## Outputs

``` 
# Route table id
```
