##################################### Route Table commissioning  ##############################################################

resource "aws_route_table" "route_table" {
  vpc_id = var.vpc_id
  route {
  cidr_block = "0.0.0.0/0"
  gateway_id             = var.gateway_id
}
  tags = merge(
    {
      "Name" = format("%s-rt", var.name)
    },
    var.tags,
  )
}
resource "aws_route_table_association" "route_table_association" {
  count          = length(var.subnet_cidr)
  subnet_id      = element(var.subnet_cidr, count.index)
  route_table_id = aws_route_table.route_table.id
}
