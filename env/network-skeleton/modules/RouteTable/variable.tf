
variable "vpc_id" {}

variable "name" {
  description = "Name of Route Table To be created "
  type        = string
  default     = "Route-Table"
}

variable "gateway_id" {}

variable "subnet_cidr" {}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
