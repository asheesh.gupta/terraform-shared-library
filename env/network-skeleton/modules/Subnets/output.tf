output "subnet_ids" {
  description = "The IDs of public subnets created"
  value       = aws_subnet.public.*.id
}
