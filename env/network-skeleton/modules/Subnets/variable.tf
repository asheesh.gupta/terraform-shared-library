################################################################### subnets ##############################################################


variable "vpc_id" {}

variable "name" {
  description = "Name of the VPC to be created"
  type        = string
  default     = "Parallel-Universe"
}

variable "sub_az" {}
variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "map_public_ip_on_launch" {
  description = "Specify true to indicate that instances launched into the subnet should be assigned a public IP address"
  type        = string
  default     = false
}

variable "map_public_ip_on_launch_for_private" {
  description = "Specify true to indicate that instances launched into the subnet should be assigned a public IP address"
  type        = string
  default     = false
}

variable "subnet_cidr" {}
