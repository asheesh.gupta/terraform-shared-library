##################################### Public subnet info ##############################################################


resource "aws_subnet" "public" {
  count                   = length(var.subnet_cidr)
  availability_zone       = element(var.sub_az, count.index) #% length(var.public_subnet_cidr))
  cidr_block              = element(var.subnet_cidr, count.index)
  vpc_id                  = var.vpc_id
  tags = merge(
    {
    Type = "Public"
    Name = format("%s-subnet-%d", var.name,count.index+1)
  },
    var.tags,
  )
}
