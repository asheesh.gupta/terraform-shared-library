# Module for Subnet

- To include this module in your code

source = " "       #path where you have stored the module

# Variables that  you need to passed while calling the VPC module

```
module "Subnets" {
  source = "./modules/Subnets/"
  vpc_id = module.vpc.vpc_id  # vpc id 
  subnet_cidr = var.private_subnet_cidr # subnets that need to pass to create subnets 
  sub_az = var.sub_az # availabilty zone 
}
```
## Otput 

```
# subnet id
```
