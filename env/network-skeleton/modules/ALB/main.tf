resource "aws_lb" "alb" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.security_group_id
  subnets            = var.subnet_id[0]
  enable_deletion_protection = true

  tags = {
    Environment = "production"
  }
}

resource "aws_alb_listener" "alb_listener" {  
  load_balancer_arn = aws_lb.alb.arn  
  port              = var.alb_listener_port  
  protocol          = var.alb_listener_protocol
  
  default_action {    
    target_group_arn = aws_alb_target_group.alb_target.arn
    type             = "forward"  
  }
}
resource "aws_alb_listener_rule" "listener_rule" { 
  listener_arn = aws_alb_listener.alb_listener.arn  
  priority     = var.priority   
  action {    
    type             = "forward"    
    target_group_arn = aws_alb_target_group.alb_target.id  
  }   
  condition {    
    field  = "path-pattern"    
    values = var.alb_path  
  }
}

resource "aws_alb_target_group" "alb_target" {  
  name     = var.target_group_name  
  port     = var.svc_port  
  protocol = "HTTP"  
  vpc_id   = var.vpc_id   
  tags = {    
    name = var.target_group_name    
  }   
   
  health_check {    
    healthy_threshold   = 3    
    unhealthy_threshold = 10    
    timeout             = 5    
    interval            = 10    
    path                = var.target_group_path    
    port                = var.target_group_port  
  }
}