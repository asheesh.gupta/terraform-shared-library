variable "alb_listener_port" {
  type = number
  default = 80
}

variable "alb_listener_protocol" {
    type = string
    default = "HTTP"
  
}
variable "security_group_id" {
    type = list
}

variable "subnet_id" {
  type =list(string)
}

variable "priority" {
    type = number
    default = 99
}
variable "target_group_name" {
    type =string
    default = "my-target-group"
}

variable "svc_port" {
    type = number
    default = 80
}

variable "vpc_id" {}

variable "target_group_path" {
  type = string
  default ="/"
}
variable "target_group_port" {
    type = number
    default = 80 
}

variable "alb_path" {
  type =list(string)
  default = ["/"]
}







