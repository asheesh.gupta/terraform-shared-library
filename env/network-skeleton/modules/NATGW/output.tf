output "ngw_id" {
  description = "The id of the NGW attached to VPC"
  value       = aws_nat_gateway.nat-gw.id
}

output "eip_for_nat_gw_id" {
  description = "The id of the eip for the NGW attached to VPC"
  value       = aws_eip.Eip.id
}