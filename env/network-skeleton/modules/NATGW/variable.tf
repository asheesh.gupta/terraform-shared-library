variable "subnet_for_nat_gw" {}

variable "name" {
  description = "Name of the VPC to be created"
  type        = string
  default     = "Parallel-Universe"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}