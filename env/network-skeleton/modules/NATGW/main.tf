################################################ Elastic IP for NAT GW ##############################################################
resource "aws_eip" "Eip" {
  vpc = true
}

################################################ Commissioning NatGW  and attaching to 1 of the public subnets ##############################################################
resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.Eip.id
  subnet_id     = element(var.subnet_for_nat_gw,1)
  tags = merge(
    {
    Name = format("%s-nat", var.name)
  },
    var.tags,
  )
}