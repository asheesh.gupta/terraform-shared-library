variable "vpc_id" {}

variable "name" {
  description = "Name of the VPC to be created"
  type        = string
  default     = "Parallel-Universe"
}