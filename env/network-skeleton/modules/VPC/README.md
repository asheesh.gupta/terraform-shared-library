#  Module for VPC

- To include this module in your code

source = " "       #path where you have stored the module

## This Vpc module does the following :-
 1. create a NAT-GATEWAY 
 2. an INTERNET-GATEWAY
 3. an eip that is used by nat-gateway

# Variables that  you need to passed while calling the VPC module

```
module "vpc" {
    source = "./modules/VPC"
    cidr_block_value = var.cidr_block  # cidr block to that is assinged to vpc
    subnet_for_nat_gw = module.Subnets.public_subnet_ids # subnet_id that is used by the natgateway
}
```

## Inputs       

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| cidr_block_value| cidr_block | list |yes| yes |
| subnet_for_nat_gateway | subnet that can be associated with  nat-gateway | List | null | yes |


## Outputs       

| Name | Description | 
|------|-------------|
| vpc_id | Id of your vpc | 
| vpc_arn | arn of the vpc |
| igw_id | INternet Gateway id|
| ngw_id |Nat Gatway id |    
| eip_for_nat_gw_id | The id of the eip for the NGW attached to VPC |


# This module has a dependency on subnet