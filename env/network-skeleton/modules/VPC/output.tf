output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.main.id
}

output "vpc_arn" {
  description = "ARN of the vpc"
  value       = aws_vpc.main.arn
}

output "owner_id" {
  description = "The owner_id of the VPC"
  value       = aws_vpc.main.owner_id
}

output "igw_id" {
  description = "The id of the IGW attached to VPC"
  value       = aws_internet_gateway.igw.id
}

output "ngw_id" {
  description = "The id of the NGW attached to VPC"
  value       = aws_nat_gateway.nat-gw.id
}

output "eip_for_nat_gw_id" {
  description = "The id of the eip for the NGW attached to VPC"
  value       = aws_eip.Eip.id
}