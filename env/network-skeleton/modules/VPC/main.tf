##################################### main VPC info ##############################################################

resource "aws_vpc" "main" {
  cidr_block                       = var.cidr_block_value
  instance_tenancy                 = var.instance_tenancy
  enable_dns_support               = var.enable_dns_support
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_classiclink               = var.enable_classiclink
  enable_classiclink_dns_support   = var.enable_classiclink_dns_support
  
  tags = merge(
    {
      "Name" = format("%s", var.name)
    },
    var.vpc_tags,
  )
}


######################################## IGW ######################################################################

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

}


################################################ Elastic IP for NAT GW ##############################################################
resource "aws_eip" "Eip" {
  vpc = true
}

################################################ Commissioning NatGW  and attaching to 1 of the public subnets ##############################################################
resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.Eip.id
  subnet_id     = element(var.subnet_for_nat_gw,1)
  tags = merge(
    {
    Name = format("%s-nat", var.name_nat)
  },
    var.tags,
  )
}




