resource "aws_security_group" "Security_group" {
  name        = var.name_sg
  description = var.name_description
  vpc_id      = var.vpc_id

  tags = {
    Name = var.tag_name
  }
}

resource "aws_security_group_rule" "My_security_group" {
  type              = "ingress"
  from_port         = var.from_port
  to_port           = var.to_port
  protocol          = var.protocol_name
  cidr_blocks       = var.cidr_blocks
  security_group_id = aws_security_group.Security_group.id
}
