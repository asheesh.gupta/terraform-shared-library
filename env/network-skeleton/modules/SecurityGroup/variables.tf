variable "vpc_id" { }

variable "cidr_blocks" {
    type = list(string)
}

variable "name_sg" {}

variable "name_description" {}

variable "to_port" {}

variable "from_port" {}

variable "protocol_name" {}

variable "tag_name" {}

