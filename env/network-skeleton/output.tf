output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc
}

output "igw_id" {
  description = "The id of the IGW attached to VPC"
  value       = module.vpc.igw_id
}


output "arn" {
  description = "The arn of the VPC"
  value       = module.vpc.vpc_arn
}

output "owner_id" {
  description = "The owner_id of the VPC"
  value       = module.vpc.owner_id
}


output "public_subnet_ids" {
  description = "The IDs of public subnets created"
  value       = module.Subnets.subnet_ids
}

output "private_subnet_ids" {
  description = "The IDs of private subnets created"
  value       = module.PrivateSubnets.subnet_ids
}


#output "default_route_table_id" {
#  description = "The default_route_table_id of the VPC"
#  value       = module.RouteTable.default_route_table_id
#}

output "main_route_table_id" {
  description = "The main_route_table_id of the VPC"
  value       = module.publicRouteTable.main_route_table_id
}

output "main_route_private_table_id" {
  description = "The main_route_table_id of the VPC"
  value       = module.privateRouteTable.main_route_table_id
}

output "ngw_id" {
  description = "The id of the NGW attached to VPC"
  value       = module.vpc.ngw_id
}

output "eip_for_nat_gw_id" {
  description = "The id of the eip for the NGW attached to VPC"
  value       = module.vpc.eip_for_nat_gw_id
}