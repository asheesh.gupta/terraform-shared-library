variable "private_subnet_cidr" {
  description = "Cidr for private subnet"
  type        = list(string)
  default     = ["192.168.56.0/24", "192.168.58.0/24", "192.168.60.0/24", "192.168.62.0/24", "192.168.64.0/24", "192.168.66.0/24", "192.168.68.0/24", "192.168.70.0/24", "192.168.72.0/24", "192.168.74.0/24", "192.168.78.0/24", "192.168.76.0/24"]
}

variable "public_subnet_cidr" {
  description = "Cidr for public subnet"
  type        = list(string)
  default     = ["192.168.57.0/24", "192.168.59.0/24", "192.168.61.0/24"]
}


variable "sub_az" {
  description = "CIDR of public subnet"
  type        = list(string)
  default     = ["ap-south-1a", "ap-south-1b", "ap-south-1c"]
}

variable "cidr_block" {
  description = "CIDR "
  type        = string
  default     = "192.168.0.0/16"
}


variable "securityGroupVar" {
  description = "CIDR "
  type        = list(string)
  default     = ["1.1.0.0/16"]

}

variable "routeTable" {
  type    = string
  default = "routeTable-Terraform"
}

variable "name_sg" {
  type    = string
  default = "my-security-group"
}

variable "name_description" {
  type    = string
  default = "sg created by terraform"
}

variable "to_port" {
  type    = number
  default = 22
}

variable "from_port" {
  type    = number
  default = 22
}

variable "protocol_name" {
  type    = string
  default = "tcp"
}

variable "tag_name" {
  type    = string
  default = "My-sg"
}